﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : Singleton<GameController> {


	[SerializeField]
	List<string> _levelsnames;

	[SerializeField]
	List<LevelDetails> _levels;

	[SerializeField]
	MeshRenderer _attractor;

	[SerializeField]
	float _fadeTime;

	[SerializeField]
	int _currentLevel;

	[SerializeField]
	public List<GameObject> _enemies;

	[SerializeField]
	GameObject _endLevelCanvas;

	[SerializeField]
	Image _fadeImage;


	public IEnumerator GotoLevel(string _name, int _level){
		print ("Going to: " + _name);
		_currentLevel = _level;
		Pause (false);
		PlayerController.Instance.OnMenuLoad ();
		StartCoroutine (FadeInImage (_fadeImage));

		yield return new WaitForSeconds (_fadeTime);

		if (_levelsnames.Contains (_name)) {
			SceneManager.LoadScene (_name, LoadSceneMode.Single);
		}
		if (_name == "Level") {
			StartCoroutine (FadeInLevel ());
			PlayerController.Instance.OnLevelLoad ();
		}
		//Scene _prevScene = SceneManager.GetActiveScene ();

		//SceneManager.SetActiveScene(_nextScene);	
		//SceneManager.UnloadSceneAsync (_prevScene);
		StartCoroutine (FadeOutImage (_fadeImage));

		yield return null;
	}

	IEnumerator FadeInLevel(){	
		_endLevelCanvas.SetActive (false);
		yield return new WaitForSeconds (_fadeTime);
		_attractor = GameObject.Find ("Attractor").GetComponent<MeshRenderer> ();
		LoadLevel (_currentLevel);
	}

	void LoadLevel(int level){
		Pause (false);
		PlayerController.Instance._menuControl = false;

		if(_levels[_currentLevel] != null){
			_attractor.material = _levels [_currentLevel]._levelMaterial;
			for (int xx = 0; xx < _levels [_currentLevel]._enemies.Length; xx++) {
				print (_levels [_currentLevel]._enemies.Length);
				for (int yy = 0; yy < _levels [_currentLevel]._numberOfEnemies[xx]; yy++) {
					print (_levels [_currentLevel]._numberOfEnemies [xx]);
					GameObject _go = GameObject.Instantiate (_levels [_currentLevel]._enemies [xx]._model, new Vector3 (GetPosition (), GetPosition (), GetPosition ()), Quaternion.identity, _attractor.transform);
					_go.GetComponent<EnemyController> ().Setup (_levels[_currentLevel]._enemies[xx]);
					_enemies.Add (_go);
				}
			}
		}
	}

	public void OnEnemyDie(GameObject _GO){
		_enemies.Remove (_GO);
		if (_enemies.Count == 0) {
			_endLevelCanvas.SetActive (true);
			Pause (true);
			Cursor.lockState = CursorLockMode.None;
		}
	}

	public void Pause(bool _pause){
		if (_pause){
			Time.timeScale = 0;
		}
		else
			Time.timeScale = 1;
	}

	public void NextLevel(){
		LoadLevel (++_currentLevel);
		_endLevelCanvas.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void PrevLevel(){
		LoadLevel (--_currentLevel);
		_endLevelCanvas.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void RestartLevel(){
		LoadLevel(_currentLevel);
		_endLevelCanvas.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void GotoMainMenu(){
		StartCoroutine( GotoLevel ("MainMenu", 0));
		_endLevelCanvas.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
	}
	float GetPosition(){
		float i = Random.Range (-1, 1);

		return i;
	}


	void Update(){
		if (Input.GetKeyDown (KeyCode.Space))
			GotoMainMenu();
	}



	IEnumerator FadeInImage(Image _image){

		//Pause (true);
		PlayerController.Instance._playerControl = false;

		print ("Fading in : " + _image.name);

		float t = 0f;

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;
			_image.color = new Color (_image.color.r, _image.color.g, _image.color.b, Mathf.Lerp (0f, 1f, t));

			yield return null;
		}
	}

	IEnumerator FadeOutImage(Image _image){

		print ("Fading out : " + _image.name);

		float t = 0f;

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;
			_image.color = new Color (_image.color.r, _image.color.g, _image.color.b, Mathf.Lerp (1f, 0f, t));

			yield return null;
		}
		PlayerController.Instance._playerControl = true;
		//Pause (false);
	}
}
