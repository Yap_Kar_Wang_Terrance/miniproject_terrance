﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
	
	[SerializeField]
	Transform _menuCanvas;

	[SerializeField]
	Transform _playerTransform;

	[SerializeField]
	Animator _myAnimator;

	[SerializeField]
	GameManager _gameManager;

	[SerializeField]
	Image _testimage;

	const float _fadeTime = 5f;
	const float smoothness = 0.02f;

	void Start(){
		StartCoroutine (FadeOut (_testimage));
	}

	void FixedUpdate(){
		//_menuCanvas.LookAt (_playerTransform.position);
		//_menuCanvas.transform.eulerAngles = new Vector3 (0, _menuCanvas.transform.eulerAngles.y, 0);
	}

	void Pressed(string name){
	//	print (name);
	//	_myAnimator.SetTrigger (name);
	}

	public void OnAnimationEnd(){
		
	}

	IEnumerator FadeOut(Image _image){

		float t = 0;
		float increment = smoothness / _fadeTime;
		Color _color = _image.color	;
		Color _startColor = _image.color;
		_color.a = 0;
		while (t < 1) {
			print (t);
			_image.color = Color.Lerp(_startColor, _color,t);
			t += increment;
			yield return null;
		}
	}

}
