﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
	
	[SerializeField]
	Transform _menuCanvas;

	[SerializeField]
	Transform _playerTransform;

	[SerializeField]
	Animator _myAnimator;

	[SerializeField]
	GameManager _gameManager;

	[SerializeField]
	Vector3 _fadeEndLocation = new Vector3(0, -9,0);

	[SerializeField]
	Image _testimage;

	[SerializeField]
	const float _fadeTime = 2f;

	void Start(){
		StartCoroutine (FadeOut (_testimage));
	}

	void FixedUpdate(){
		//_menuCanvas.LookAt (_playerTransform.position);
		//_menuCanvas.transform.eulerAngles = new Vector3 (0, _menuCanvas.transform.eulerAngles.y, 0);
	}

	void Pressed(string name){
	//	print (name);
	//	_myAnimator.SetTrigger (name);
	}

	public void OnAnimationEnd(){
		
	}

	IEnumerator FadeOut(Image _image){

		float t = 0f;
		print (t);

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;
			print (t);
			_image.transform.localPosition = new Vector3 (0, Mathf.Lerp (_image.transform.position.y, _fadeEndLocation.y, t), 0);
			yield return null;
		}
	}

}
