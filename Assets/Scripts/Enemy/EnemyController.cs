﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ENEMY_STATES
{
	idle,
	patrol,
	attack,
	dead
}

public class EnemyController : MonoBehaviour {

	[SerializeField]
	private float _speed;
	[SerializeField]
	private float _changeTime;

	[SerializeField]
	private int _health;
	[SerializeField]
	MissileDetails _missileDetails;

	[SerializeField]
	ENEMY_STATES _currentState = ENEMY_STATES.idle;

	[SerializeField]
	Transform _missleStartPos;

	public void Setup(EnemyDetails enemyDetails){
		_speed = enemyDetails._speed;
		_changeTime = enemyDetails._changeTime;
		_health = enemyDetails._health;
		_missileDetails = enemyDetails._missile;

		StartCoroutine (EnemyMovement ());
	}

	void Hit(int damage){
		print ("Took : " + damage);
		_health -= damage;
	}

	void OnCollisionEnter(Collision col){
		if (col.collider.CompareTag ("Enemy")) {
			Vector3 newDirection = new Vector3 (0, Random.Range(0, 180), 0);
			transform.Rotate (newDirection);
		}
	}

	IEnumerator EnemyMovement(){
		float t = 0;
		while (_health > 0) {
			t += Time.deltaTime;

			switch (_currentState) {
			case ENEMY_STATES.idle:
				if (t > _changeTime) {
					_currentState = (Random.Range (0, 2) == 0) ? ENEMY_STATES.attack : _currentState = ENEMY_STATES.patrol;
					t = 0;
				}
				break;
			case ENEMY_STATES.patrol:
				transform.position += transform.forward * _speed * Time.deltaTime;
				if (t > _changeTime) {
					_currentState = (Random.Range (0, 2) == 0) ? ENEMY_STATES.attack : _currentState = ENEMY_STATES.idle;
					t = 0;
				}
				break;
			case ENEMY_STATES.attack:
				//Wait for missle to be put
				GameObject _missile = Instantiate (_missileDetails.model, _missleStartPos.position, _missleStartPos.rotation, transform.GetChild(0).transform);
				t = 0f;

				while (t < 1f) {
					t += Time.deltaTime / 2f;

					_missile.transform.localEulerAngles = new Vector3 (Mathf.Lerp (0f, 90f, t), 0f, 0f);

					yield return null;
				}

				_missile.GetComponentInChildren<MissileController> ().Setup (_missileDetails);
				//spawn missle
				_currentState = (Random.Range (0, 2) == 0) ? ENEMY_STATES.patrol : _currentState = ENEMY_STATES.idle;
				//missle code
				t = 0f;
				break;
			}


			print (_currentState);

			yield return null;
		}

		//Play Dead Animation
		GameController.Instance.OnEnemyDie(this.gameObject);
		Destroy(this.gameObject);


	}
}
