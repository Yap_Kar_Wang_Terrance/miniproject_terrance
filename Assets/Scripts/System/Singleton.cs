﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T> {

	void Awake(){
		if(_instance != null)
			Destroy(gameObject);
		_instance = this as T;

		print (_instance.name);

		DontDestroyOnLoad (gameObject);
	}

	static T _instance;

	public static T Instance {
		get {
			//print ("Instance is not Null");
			if (_instance == null) {
				//print ("Instance is null");
				T __object = FindObjectOfType (typeof(T)) as T;
				if (__object != null) {
					//print ("Instance is Object");
					_instance = __object;
				} else {
					//print ("Instance is Created");
					var __go = new GameObject (string.Format ("_(0)", typeof(T), typeof(T)));
					_instance = __go.GetComponent<T> ();
				}
			}
			return _instance;
		}
		private set {
			_instance = value as T;
		}
	}
}
