﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : Singleton<PlayerController>
{
	#region Objects
	[SerializeField]
	Transform _myTransform;

	[SerializeField]
	Transform _cameraTransform;

	[SerializeField]
	GameObject _hud;

	[Header ("Gun")]
	[SerializeField]
	AudioSource _gunAudioSource;

	[SerializeField]
	Transform _gunTransform;

	[SerializeField]
	AudioClip _pew;

	[SerializeField]
	ParticleSystem _flare;


	[Header ("Player Ui")]
	[SerializeField]
	Text _ammoText;
	[SerializeField]
	Image _ammoImage;

	[SerializeField]
	Text _healthText;
	[SerializeField]
	Image _healthImage;

	[SerializeField]
	GameObject[] _hudObjects;

	[SerializeField]
	private float _healthWidth;
	#endregion

	#region Varables

	[Header ("Unity Value")]
	[SerializeField]
	public bool _menuControl;

	[SerializeField]
	bool _menuWait;

	[SerializeField]
	bool _unityController;

	[SerializeField]
	public bool _playerControl;

	[Header ("Player Stats")]
	[SerializeField]
	PlayerDetails _playerDetails;

	[SerializeField]
	public float _speed;

	[SerializeField]
	private float _reloadTime;

	[SerializeField]
	private bool _reloading;

	[SerializeField]
	private float _currentFire;

	[SerializeField]
	private float _fireRate;

	[SerializeField]
	private int _currentBullet;

	[SerializeField]
	private int _maxBullet;

	[SerializeField]
	private int _currentHealth;

	[SerializeField]
	private int _maxHealth;

	[SerializeField]
	private int _bulletDamage;

	[SerializeField]
	private int _nukeDamage;

	[SerializeField]
	private bool _reloadingNuke;

	[SerializeField]
	private float _nukeReloadTime;

	#endregion

	RaycastHit hit;

	void Start ()
	{

		Cursor.lockState = CursorLockMode.Locked;	

		SetupPlayerStats ();

		//Initz
		_currentBullet = _maxBullet;
		_ammoText.text = _currentBullet.ToString();
		_currentHealth = _maxHealth;
		_healthText.text = (_currentHealth).ToString ();

		_healthWidth = _healthImage.rectTransform.sizeDelta.x;
	}

	void SetupPlayerStats ()
	{
		if (_playerDetails._firstRun) {
			_playerDetails.Reset();
			_playerDetails._firstRun = false;
		}

		_reloadTime = _playerDetails._reloadTime;
		_fireRate = _playerDetails._fireRate;
		_maxBullet = _playerDetails._maxBullet;
		_maxHealth = _playerDetails._maxHealth;
		_nukeReloadTime = _playerDetails._nukeReloadTime;
		_bulletDamage = _playerDetails._bulletDamage;
		_nukeDamage = _playerDetails._nukeDamage;
	}

	void Reset(){
		_currentBullet = _maxBullet;
		_currentHealth = _maxHealth;
		_reloading = false;
		_reloadingNuke = false;
	}

	void FixedUpdate ()
	{
		if (!_playerControl)
			return;

		if (_unityController) {
			float _vertical;
			if (Input.GetKey (KeyCode.Q))
				_vertical = 1;
			else if (Input.GetKey (KeyCode.E))
				_vertical = -1;
			else
				_vertical = 0;
			_myTransform.Translate (new Vector3 (Input.GetAxis ("Horizontal"), _vertical, Input.GetAxis ("Vertical")) * _speed * Time.deltaTime);

			_myTransform.Rotate (new Vector3 ((Input.GetAxis ("Mouse Y") * -1), Input.GetAxis ("Mouse X"), 0) * _speed * 100 * Time.deltaTime);
		}

		Physics.Raycast (_myTransform.position, _myTransform.forward, out hit);

		if (Input.GetAxis ("Fire1") == 1) {
			if (!_menuControl)
				Fire ();
			else if (!_menuWait) {
				hit.collider.SendMessageUpwards ("Pressed", hit.collider.name);
				StartCoroutine (MenuWait ());
			}
			
		} else {
			_currentFire = 0;
		}

		if (Input.GetAxis ("Fire2") == 1 && !_reloadingNuke) {
			if (!_menuControl) {
				StartCoroutine (ReloadNuke ());
				Nuke ();
			}
		}


		if (Input.GetKey (KeyCode.F))
			TakeDamage (1);
	}

	#region Fire

	public bool Fire ()
	{
		//Shoot
		_gunTransform.localEulerAngles -= new Vector3 (0f, 0f, 45f / _fireRate * Time.deltaTime);
		_currentFire += Time.deltaTime;
		if (!_reloading && (_currentFire > _fireRate)) {
			if (hit.collider != null) {
				if (hit.collider.CompareTag ("Enemy"))
					hit.collider.SendMessage ("Hit", _bulletDamage);
			}
			if (!_flare.IsAlive ())
				_flare.Play ();
				
			//PlaySound
			_gunAudioSource.clip = _pew;
			_gunAudioSource.Play ();
			_currentBullet--;
				
			if (_ammoImage != null && _ammoText != null) {
				float _fillAmount = (float)_currentBullet / (float)_maxBullet;
				_ammoImage.fillAmount = _fillAmount;
				_ammoText.text = _currentBullet.ToString();
			}

			_currentFire = 0;
			return true;
		} else if (_currentBullet < 1) {
			StartCoroutine (Reload ());
			return false;
		
		}
		return false;
	}

	public bool Nuke(){

		foreach(GameObject GO in GameObject.FindGameObjectsWithTag ("Enemy") ){
			GO.SendMessage ("Hit", _nukeDamage);
		}
		print ("KaBoomz");
		return true;
	}

	#endregion

	public void TakeDamage (int damage)
	{
		_currentHealth -= damage;
		if (_currentHealth < 0)
			_currentHealth = 0;

		float _fillAmount = _healthWidth * ((float)_currentHealth / (float)_maxHealth);

		_healthImage.rectTransform.sizeDelta = new Vector2(_fillAmount, _healthImage.rectTransform.sizeDelta.y);

		_healthText.text = _currentHealth.ToString ();
	}

	IEnumerator Reload ()
	{
		_reloading = true;
		yield return new WaitForSeconds (_reloadTime);
		_reloading = false;
		_currentBullet = _maxBullet; 

		if (_ammoImage != null && _ammoText != null){
			float _fillAmount = _currentBullet / _maxBullet;

			_ammoImage.fillAmount = _fillAmount;
			_ammoText.text = _currentBullet.ToString();
		}

	}

	IEnumerator ReloadNuke(){
		_reloadingNuke = true;
		yield return new WaitForSeconds (_nukeReloadTime);
		_reloadingNuke = false;
	}

	IEnumerator MenuWait(){
		_menuWait = true;
		yield return new WaitForSeconds (1f);
		_menuWait = false;
	}

	public void OnMenuLoad(){
		for (int i = 0; i < _hudObjects.Length; i++) {
			_hudObjects [i].SetActive (false);
		}
	}

	public void OnLevelLoad(){
		for (int i = 0; i < _hudObjects.Length; i++) {
			_hudObjects [i].SetActive (true);
		}

		Reset ();
	}
}
