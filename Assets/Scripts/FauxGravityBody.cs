﻿using UnityEngine;
using System.Collections;

public class FauxGravityBody : MonoBehaviour {
	private FauxGravityAttactor attractor;

	// Use this for initialization
	public void Awake () 
	{
		//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		GetComponent<Rigidbody>().useGravity = false;
		attractor = GameObject.Find ("Attractor").GetComponent<FauxGravityAttactor>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{	
		if (attractor != null)
			attractor.Attract (transform, true);
		else
			attractor = GameObject.Find ("Attractor").GetComponent<FauxGravityAttactor> ();
	}
}
