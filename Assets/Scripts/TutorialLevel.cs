﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialLevel : MonoBehaviour
{
	[SerializeField]
	Canvas _tutorialCanvas;
	[SerializeField]
	Text _instructionText;
	[SerializeField]
	Text _progressText;
	[SerializeField]
	GameObject _tutorialEnemy;

	[SerializeField]
	float _fadeTime = 2f;

	void Start(){

		_instructionText.color = new Color (_instructionText.color.r, _instructionText.color.g, _instructionText.color.b, Mathf.Lerp (0f, 1f, 0));
		_progressText.color = new Color (_instructionText.color.r, _instructionText.color.g, _instructionText.color.b, Mathf.Lerp (0f, 1f, 0));
		_tutorialCanvas.worldCamera = Camera.main;
		StartCoroutine (TutorialProcess ());

		//_instructionText.text = "Welcome to the Tutorial";
	}

	IEnumerator FadeInText(Text _text){

		print ("Fading in : " + _text.name);

		float t = 0f;

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;
			_text.color = new Color (_text.color.r, _text.color.g, _text.color.b, Mathf.Lerp (0f, 1f, t));

			yield return null;
		}
	}

	IEnumerator FadeOutText(Text _text){
		
		print ("Fading out : " + _text.name);

		float t = 0f;

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;
			_text.color = new Color (_text.color.r, _text.color.g, _text.color.b, Mathf.Lerp (1f, 0f, t));

			yield return null;
		}
	}

	IEnumerator TutorialProcess(){
		StartCoroutine(FadeInText (_instructionText));
		yield return new WaitForSeconds (_fadeTime);
		StartCoroutine(FadeOutText (_instructionText));
		yield return new WaitForSeconds (_fadeTime);
		_instructionText.text = "Move Around";
		_progressText.text = "Moved : 0/3";
		StartCoroutine(FadeInText (_instructionText));
		StartCoroutine(FadeInText (_progressText));
		yield return new WaitForSeconds (_fadeTime);
		float _distanceMoved = 0;
		Vector3 playerPrevPos = new Vector3();
		while(_distanceMoved < 3){
			_distanceMoved += (PlayerController.Instance.transform.position - playerPrevPos).magnitude;
			playerPrevPos = PlayerController.Instance.transform.position;
			_progressText.text = "Moved : " + Mathf.RoundToInt (_distanceMoved) + "/3";
			yield return null;
		}
		StartCoroutine(FadeOutText (_instructionText));
		StartCoroutine(FadeOutText (_progressText));
		yield return new WaitForSeconds (_fadeTime);
		_instructionText.text = "Hold Left Click To Shoot";
		_progressText.text = "Shot : 0/5";
		StartCoroutine(FadeInText (_instructionText));
		StartCoroutine(FadeInText (_progressText));
		yield return new WaitForSeconds (_fadeTime);
		int _shotFired = 0;
		while(_shotFired < 5){
			if (Input.GetAxis ("Fire1") == 1) {
				if (PlayerController.Instance.Fire ())
					_shotFired++;
			}
			_progressText.text = "Shot : " + _shotFired + "/5";
			yield return null;
		}
		StartCoroutine(FadeOutText (_instructionText));
		StartCoroutine(FadeOutText (_progressText));
		yield return new WaitForSeconds (_fadeTime);

		_instructionText.text = "Right Click To Shoot";
		_progressText.text = "Nuked : 0/1";
		StartCoroutine(FadeInText (_instructionText));
		StartCoroutine(FadeInText (_progressText));
		yield return new WaitForSeconds (_fadeTime);
		while (Input.GetAxis ("Fire2") != 1 && _progressText.text != "Nuked : 1/1") {
			yield return new WaitUntil (() => Input.GetAxis ("Fire2") == 1);
			PlayerController.Instance.Nuke ();
			_progressText.text = "Nuked : 1/1";
		}
		StartCoroutine(FadeOutText (_instructionText));
		StartCoroutine(FadeOutText (_progressText));
		yield return new WaitForSeconds (_fadeTime);
		_instructionText.text = "Find and kill The Enemy";
		StartCoroutine(FadeInText (_instructionText));
		yield return new WaitForSeconds (_fadeTime);
		PlayerController.Instance._menuControl = false;
		_tutorialEnemy.SetActive (true);
		yield return new WaitUntil (() => _tutorialEnemy == null);
		StartCoroutine(FadeOutText (_instructionText));
		yield return new WaitForSeconds (_fadeTime);
		GameController.Instance.SendMessage ("GotoLevel", "Level1");
		print ("Tutorial Completed");
	}
}

	/*
	public Text instructionText;
	public Toggle toggleMoved;
	public Toggle toggleShot;
	public Toggle toggleEnemy;
	public Toggle toggleNuke;
	public GameObject tutorialEnemy;

	private bool shot;
	private bool enemyKilled;
	private float distanceMoved;
	private Vector3 playerPrevPos;
	private GameObject mainCamera;
	private AudioSource audioSource;
	private float elapsedTime;
	private int tutorialProgress;
	private int shotFired;
	PlayerController _playerController;

	void Start ()
	{
		mainCamera = GameObject.Find ("Main Camera");
		playerPrevPos = mainCamera.transform.position;
		instructionText.canvasRenderer.SetAlpha (0.0f);

		tutorialEnemy.SetActive (false);
		toggleMoved.gameObject.SetActive (false);
		toggleShot.gameObject.SetActive (false);
		toggleEnemy.gameObject.SetActive (false);
		toggleNuke.gameObject.SetActive (false);

		audioSource = this.GetComponent<AudioSource> ();

		_playerController = PlayerController.Instance.GetComponent<PlayerController>();
	}





	void FixedUpdate ()
	{
		switch(tutorialProgress)
		{
		case 0:
			elapsedTime += Time.deltaTime;

			if (instructionText.canvasRenderer.GetAlpha () == 0.0f) 
			{
				SetAlpha (1f, 1.0f);
				elapsedTime = 0;
			}

			if (elapsedTime < 3) 
			{
				return;
			}

			if (instructionText.canvasRenderer.GetAlpha () >= 0.8f) 
			{
				SetAlpha (0.0f, 1.0f);
				elapsedTime = 0;
				tutorialProgress++;
			}
			break;
		case 1:
			if (instructionText.canvasRenderer.GetAlpha () == 0.0f) 
			{
				instructionText.text = "Move around";
				SetAlpha (1.0f, 1.0f);
			}

			if (instructionText.canvasRenderer.GetAlpha () == 1.0f) 
			{
				toggleMoved.gameObject.SetActive (true);

				distanceMoved += (mainCamera.transform.position - playerPrevPos).magnitude;
				playerPrevPos = mainCamera.transform.position;
			}

			toggleMoved.transform.Find ("Label").GetComponent<Text> ().text = "Moved: " + Mathf.RoundToInt(distanceMoved) + "/3";

			if (distanceMoved > 3) 
			{
				if (!shot) 
				{
					audioSource.Play ();
					shot = true;
					SetAlpha (0.0f, 1.0f);
					toggleMoved.isOn = shot;
				}

				if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
				{
					tutorialProgress++;
					shot = false;
				}
			}
			break;
		case 2:
			if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
			{
				SetAlpha (1.0f, 1.0f);
				toggleMoved.gameObject.SetActive (false);
				toggleShot.gameObject.SetActive (true);
				instructionText.text = "Left Click to Shoot";
			}

			if (Input.GetAxis ("Fire1") != 0) 
			{
				//print (_playerController.Fire());

				print (_playerController._currentFire);
				print (_playerController._fireRate);

				//elapsedTime += Time.deltaTime;

				if (_playerController.Fire ())
					shotFired++;
				
				toggleShot.transform.Find ("Label").GetComponent<Text> ().text = "Shot: " + shotFired + "/300";

				if (elapsedTime > 3) 
				{
					audioSource.Play ();
					shot = true;
					toggleShot.isOn = shot;
					SetAlpha (0.0f, 1.0f);
					elapsedTime = 0;
					tutorialProgress++;
				}
			}
			break;
		case 3:
			if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
			{
				SetAlpha (1.0f, 1.0f);
				toggleShot.gameObject.SetActive (false);
				toggleNuke.gameObject.SetActive (true);
				instructionText.text = "Right Click to Nuke";
			}

			if (Input.GetAxis ("Fire2") != 0) 
			{
				if (!shot) 
				{
					audioSource.Play ();
				}

				shot = true;
				toggleNuke.isOn = shot;
				SetAlpha (0.0f, 1.0f);
				tutorialProgress++;
			}
			break;
		case 4:
			if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
			{
				toggleNuke.gameObject.SetActive (false);
				SetAlpha (1.0f, 1.0f);
				instructionText.text = "Find and Destroy the Enemy";
				tutorialEnemy.SetActive (true);
				toggleEnemy.gameObject.SetActive (true);
				//GameController.instance.CheckEnemyLeft ();
			}

			if (tutorialEnemy == null) 
			{
				if (!enemyKilled) 
				{
					audioSource.Play ();
				}

				enemyKilled = true;
				toggleEnemy.isOn = true;
				SetAlpha (0.0f, 1.0f);
				tutorialProgress++;
			}
			break;
		case 5:
			if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
			{
				toggleEnemy.gameObject.SetActive (false);
				SetAlpha (1.0f, 1.0f);
				instructionText.text = "You have completed the Tutorial!!";
				tutorialProgress++;
			}
			break;
		case 6:
			if (instructionText.canvasRenderer.GetAlpha () >= 0.8f) 
			{
				SetAlpha (0.0f, 1.0f);
			}
			if (instructionText.canvasRenderer.GetAlpha () <= 0.1f) 
			{
				tutorialProgress++;
				//GameController.instance.CheckEnemyLeft ();
			}
			break;
		case 7:
			SceneManager.LoadScene ("Level1");
			break;
		}
	}

	void SetAlpha (float alpha, float fadeDuration)
	{
		instructionText.CrossFadeAlpha (alpha, fadeDuration, false);
	}
}
*/