﻿using UnityEngine;
using System.Collections;

public class SplashController : MonoBehaviour {


	[SerializeField]
	GameController _gameController;
	[SerializeField]
	PlayerController _playerController;

	IEnumerator Start(){

		//_gameController = GameController.Instance;
		//_playerController = PlayerController.Instance;

		_gameController.gameObject.SetActive (true);
		_playerController.gameObject.SetActive (true);

		_gameController.StartCoroutine(_gameController.GotoLevel("MainMenu", 0));
		yield return null;
	}
}
