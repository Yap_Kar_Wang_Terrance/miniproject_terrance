﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
public class EnemyAsset
{
	[MenuItem("Assets/Create/EnemyDetails")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<EnemyDetails> ();
	}
}
#endif
