﻿using UnityEngine;

public class EnemyDetails : ScriptableObject {

	[SerializeField]
	public float _speed;
	[SerializeField]
	public float _changeTime;
	[SerializeField]
	public int _health;
	[SerializeField]
	public GameObject _model;
	[SerializeField]
	public MissileDetails _missile;

}
