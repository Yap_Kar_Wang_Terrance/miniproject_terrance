﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
public class LevelAsset
{
	[MenuItem("Assets/Create/LevelDetails")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<LevelDetails> ();
	}
}
#endif
