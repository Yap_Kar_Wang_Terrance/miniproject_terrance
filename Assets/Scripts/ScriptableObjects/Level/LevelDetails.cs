﻿using UnityEngine;

public class LevelDetails : ScriptableObject {

	[SerializeField]
	public int _level;

	[SerializeField]
	public EnemyDetails[] _enemies;
	[SerializeField]
	public int[] _numberOfEnemies;


	[SerializeField]
	public Material _levelMaterial;
}
