﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

	[SerializeField]
	private float _speed;
	[SerializeField]
	private int _health;
	[SerializeField]
	private float _lifeTime;
	[SerializeField]
	private float _startTime;
	[SerializeField]
	private float _rotateSpeed;
	[SerializeField]
	private int _damage;
	[SerializeField]
	private GameObject _parent;
	[SerializeField]
	Transform _target;


	public void Setup(MissileDetails missileDetails){
		_parent = transform.parent.gameObject;
		_speed = missileDetails.speed;
		_health = missileDetails.health;
		_lifeTime = missileDetails.lifeTime;
		_startTime = missileDetails.startTime;
		_rotateSpeed = missileDetails.rotateSpeed;
		_damage = missileDetails.damage;

		transform.parent = null;
		Destroy (_parent);

		StartCoroutine(Shoot());
	}

	void RotateToPlayer(){
		_target.LookAt (PlayerController.Instance.transform);

		float step = _rotateSpeed * Time.deltaTime;

		transform.rotation = Quaternion.RotateTowards (transform.rotation, _target.rotation, step);
	}

	void OnTriggerEnter(Collider _col){
		print (_col.name);
		if (_col.CompareTag ("Player")) {
			_col.GetComponent<PlayerController> ().TakeDamage (_damage);
			Destroy (this.gameObject);
		}
	}

	void Hit(int damage){
		print ("Took : " + damage);
		_health -= damage;
	}

	IEnumerator Shoot(){
		
		yield return new WaitForSeconds (_startTime);

		float t = 0;

		while (t < _startTime && _health > 0) {
			transform.Translate ((Vector3.forward) * _speed * Time.deltaTime);
			t += Time.deltaTime;
			print (_health);
			yield return null;
		}

		if (_health <= 0) {
			Destroy (this.gameObject);
		}

		StartCoroutine (Chase ());
	}

	IEnumerator Chase(){

		while (_health > 0) {
			RotateToPlayer ();
			transform.Translate ((Vector3.forward) * _speed * Time.deltaTime);
			yield return null;
		}

		Destroy (this.gameObject);
	}

}
