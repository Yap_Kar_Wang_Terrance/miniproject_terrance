﻿using UnityEngine;

public class MissileDetails : ScriptableObject {

	[SerializeField]
	public int health;
	[SerializeField]
	public int damage;
	[SerializeField]
	public float speed;
	[SerializeField]
	public float lifeTime;
	[SerializeField]
	public float startTime;
	[SerializeField]
	public float rotateSpeed;

	[SerializeField]
	public GameObject model;

}
