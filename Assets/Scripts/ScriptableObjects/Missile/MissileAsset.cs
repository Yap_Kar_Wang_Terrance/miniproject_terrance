﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
public class MissileAsset
{
	[MenuItem("Assets/Create/MissileDetails")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<MissileDetails> ();
	}
}
#endif
