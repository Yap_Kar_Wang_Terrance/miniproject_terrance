﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
public class PlayerAsset
{
	[MenuItem("Assets/Create/PlayerAsset")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<PlayerDetails> ();
	}
}
#endif
