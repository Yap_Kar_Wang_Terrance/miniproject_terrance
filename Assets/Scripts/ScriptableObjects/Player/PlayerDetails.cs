﻿using UnityEngine;

[System.Serializable]
public class PlayerDetails : ScriptableObject {

	public bool _firstRun = true;

	public float _reloadTime;
	public float _nukeReloadTime;
	public float _fireRate;
	public int _maxBullet;
	public int _maxHealth;
	public int _bulletDamage;
	public int _nukeDamage;

	public const float _baseReloadTime = 2f;
	public const float _baseNukeReloadTime = 5f;
	public const float _baseFireRate = 0.5f;
	public const int _baseMaxBullet = 30;
	public const int _baseMaxHealth = 100;
	public const int _baseBulletDamage = 10;
	public const int _baseNukeDamage = 50;

	public void Reset(){
		_reloadTime = _baseReloadTime;
		_nukeReloadTime = _baseNukeReloadTime;
		_fireRate = _baseFireRate;
		_maxBullet = _baseMaxBullet;
		_maxHealth = _baseMaxHealth;
		_bulletDamage = _baseBulletDamage;
		_nukeDamage = _baseNukeDamage;
	}
}