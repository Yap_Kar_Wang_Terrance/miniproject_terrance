﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

enum MENUSTATES{
	IDLE,
	LEVELSELECT,
	OPTIONS,
	HIGHSCORE
}

public class MenuController : MonoBehaviour {
	
	[SerializeField]
	Transform _menuCanvas;

	[SerializeField]
	Transform _playerTransform;

	[SerializeField]
	Animator _myAnimator;

	[SerializeField]
	private GameController _gameManager;

	[SerializeField]
	Vector3 _fadeEndLocation = new Vector3(0, -9,0);

	[SerializeField]
	Image _testimage;

	[SerializeField]
	const float _fadeTime = 2f;

	[SerializeField]
	MENUSTATES _currentState = MENUSTATES.IDLE;

	void Awake(){
		PlayerController.Instance._menuControl = true;
		_playerTransform = PlayerController.Instance.transform;
		_gameManager = GameController.Instance;

	}

	void FixedUpdate(){
		if (_playerTransform != null) {
			_menuCanvas.LookAt (_playerTransform.position);
			_menuCanvas.transform.eulerAngles = new Vector3 (0, _menuCanvas.transform.eulerAngles.y, 0);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			Pressed ("LevelSelect");
		}
	}

	void Pressed(string name){
		switch(_currentState){
		case MENUSTATES.IDLE:
			_myAnimator.SetTrigger ("DropAnimation");
			switch (name) {
			case "LevelSelect":
				_myAnimator.SetTrigger ("LevelSelect");
				_currentState = MENUSTATES.LEVELSELECT;
				break;
			case "Options":
			
				break;
			case "Highscore":
			
				break;
			}
			break;

		case MENUSTATES.LEVELSELECT:
			print (name);
			if (name == "Tutorial") {
				StartCoroutine (_gameManager.GotoLevel (name, 0));
				return;
			}
			int _level;

			int.TryParse (name, out _level);
			print (_level);

			StartCoroutine(_gameManager.GotoLevel ("Level", _level));
			break;
		case MENUSTATES.OPTIONS:
			break;
		case MENUSTATES.HIGHSCORE:
			break;
		}
	}

	void GotoSelectLevel(){

	}

	void GotoOptions(){

	}

	void GotoHighscore(){

	}

}
